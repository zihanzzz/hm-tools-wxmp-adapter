package top.hmtools.wxmp.server.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import top.hmtools.wxmp.webpage.jsSdk.ESignType;
import top.hmtools.wxmp.webpage.jsSdk.JsConfigVO;
import top.hmtools.wxmp.webpage.jsSdk.JsSdkTools;

@Controller
public class JsSdkController extends BaseController {

	@Autowired
	private JsSdkTools jsSdkTools;
	
	@GetMapping("/wxmp/jssdk/config")
	@ResponseBody
	public JsConfigVO getJsConfig(@RequestParam("currentUrl")String urlStr){
		return this.jsSdkTools.getJsConfig(urlStr, ESignType.SHA1);
	}
}
