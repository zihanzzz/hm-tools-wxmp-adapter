package top.hmtools.httpclient;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.http.HttpHost;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.routing.HttpRoute;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.junit.Test;

public class HttpClientTest {

	private String url = "https://www.baidu.com";
	
	/**
	 * 完整使用 自定义 httphost、HttpRequest、ResponseHandler、HttpContext 发起请求
	 */
	@Test
	public void allAA(){
		//设置重试方案
		HttpRequestRetryHandlerImpl retry = new HttpRequestRetryHandlerImpl();
		
		// 设置超时时间
		RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(1000).setSocketTimeout(1000).setConnectionRequestTimeout(1000).build();
		
		//获取httpclient
		CloseableHttpClient httpClient = HttpClients.custom().setDefaultRequestConfig(requestConfig).setRetryHandler(retry).build();
		
		//设置路由请求服务器地址
		HttpHost httpHost = new HttpHost("localhost",80);
		
		//设置请求方式
		HttpRequest request = new HttpGet(url);
		
		//设置响应处理
		ResponseHandler<String> responseHandler = new ResponseHandler<String>() {
			
			@Override
			public String handleResponse(HttpResponse response) throws ClientProtocolException, IOException {
				String result = IOUtils.toString(response.getEntity().getContent(),"UTF-8").replaceAll("\n", "").replaceAll("\r","").trim().substring(0,10);
				return result;
			}
		};
		
		HttpContext context = new BasicHttpContext();
		try {
			String execute = httpClient.execute(httpHost, request, responseHandler, context);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * ResponseHandler测试，同一个httpclient循环请求同一个网址
	 */
	@Test
	public void simpleResponseHandleAA(){
		//设置重试方案
		HttpRequestRetryHandlerImpl retry = new HttpRequestRetryHandlerImpl();
		
		// 设置超时时间
		RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(1000).setSocketTimeout(1000).setConnectionRequestTimeout(1000).build();
		
		//获取httpclient
		CloseableHttpClient httpClient = HttpClients.custom().setDefaultRequestConfig(requestConfig).setRetryHandler(retry).build();
		
		HttpGet get = new HttpGet(this.url);
		ResponseHandler<String> responseHandler = new ResponseHandler<String>() {
			
			@Override
			public String handleResponse(HttpResponse response) throws ClientProtocolException, IOException {
				InputStream inputStream = response.getEntity().getContent();
				System.out.println("返回的数据流字符长度为："+inputStream.available());
				String result = IOUtils.toString(response.getEntity().getContent(),"UTF-8").replaceAll("\n", "").replaceAll("\r","").trim().substring(0,10);
				return result;
			}
		};
		
		for(int ii=0;ii<32;ii++){
			try {
				String execute = httpClient.execute(get, responseHandler);
				System.out.println(execute);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	/**
	 * 测试httpclient连接池，使用同一个httpclient循环请求同一个网址
	 */
	@Test
	public void poolingTestAA() {
		// 设置连接池的连接数
		PoolingHttpClientConnectionManager cm = new PoolingHttpClientConnectionManager();
		// Increase max total connection to 200
		cm.setMaxTotal(200);
		// Increase default max connection per route to 20
		cm.setDefaultMaxPerRoute(20);
		// Increase max connections for localhost:80 to 50 是否设置路由似乎没什么影响
		HttpHost baidu = new HttpHost("www.baidu.com", 80);//在 httpclient.execute中使用，可提高速度
//		cm.setMaxPerRoute(new HttpRoute(baidu), 50);//可选

		// 获取http客户端
		CloseableHttpClient httpClient = HttpClients.custom().setConnectionManager(cm).build();

		long start = System.currentTimeMillis();

		// 循环请求统一个网址，使用同一个http客户端
		HttpGet get = new HttpGet(url);
		CloseableHttpResponse httpResponse = null;
		for (int ii = 0; ii < 32; ii++) {
			try {
				long localStart = System.currentTimeMillis();
				httpResponse = httpClient.execute(baidu,get);//速度较快
//				httpResponse = httpClient.execute(get);//速度较慢
				System.out.println(ii+"、httpClient实例是："+httpClient+"，目标网站返回http状态为：" + httpResponse.getStatusLine().getStatusCode() + "，历时"
						+ (System.currentTimeMillis() - localStart) + "毫秒");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally {
				if(httpResponse!=null){
					try {
						httpResponse.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}
		System.out.println("总历时" + (System.currentTimeMillis() - start) + "毫秒");
	}

	/**
	 * 测试httpclient连接池，使用不同的HttpClientBuilder生成不同的httpclient循环请求同一个网址
	 */
	@Test
	public void poolingTestCC() {
		// 设置连接池的连接数
		PoolingHttpClientConnectionManager cm = new PoolingHttpClientConnectionManager();
		// Increase max total connection to 200
		cm.setMaxTotal(200);
		// Increase default max connection per route to 20
		cm.setDefaultMaxPerRoute(20);
		// Increase max connections for localhost:80 to 50 是否设置路由似乎没什么影响
		HttpHost baidu = new HttpHost(url.replace("http://", ""), 80);
		cm.setMaxPerRoute(new HttpRoute(baidu), 50);

		// 获取http客户端

		long start = System.currentTimeMillis();

		// 循环请求统一个网址，使用同一个http客户端
		HttpGet get = new HttpGet(url);
		CloseableHttpResponse httpResponse = null;
		for (int ii = 0; ii < 32; ii++) {
			try {
				CloseableHttpClient httpClient = HttpClients.custom().setConnectionManager(cm).build();
				long localStart = System.currentTimeMillis();
				httpResponse = httpClient.execute(baidu,get);
				System.out.println(ii+"、httpClient实例是："+httpClient+"，目标网站返回http状态为：" + httpResponse.getStatusLine().getStatusCode() + "，历时"
						+ (System.currentTimeMillis() - localStart) + "毫秒");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally {
				if(httpResponse!=null){
					try {
						httpResponse.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}
		System.out.println("总历时" + (System.currentTimeMillis() - start) + "毫秒");
	}

	/**
	 * 测试httpclient连接池，使用同一个HttpClientBuilder生成不同的httpclient循环请求同一个网址
	 */
	@Test
	public void poolingTestBB() {
		// 设置连接池的连接数
		PoolingHttpClientConnectionManager cm = new PoolingHttpClientConnectionManager();
		// Increase max total connection to 200
		cm.setMaxTotal(200);
		// Increase default max connection per route to 20
		cm.setDefaultMaxPerRoute(20);
		// Increase max connections for localhost:80 to 50 是否设置路由似乎没什么影响
		HttpHost baidu = new HttpHost(url, 80);
		cm.setMaxPerRoute(new HttpRoute(baidu), 50);
		
		// 设置超时时间
		RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(1000).setSocketTimeout(1000).setConnectionRequestTimeout(1000).build();
		
		//设置重试方案
		HttpRequestRetryHandlerImpl retry = new HttpRequestRetryHandlerImpl();

		// 获取http客户端
		HttpClientBuilder httpClientBuilder = HttpClients.custom().setConnectionManager(cm).setDefaultRequestConfig(requestConfig).setRetryHandler(retry);

		long start = System.currentTimeMillis();

		// 循环请求统一个网址，使用同一个http客户端
		HttpGet get = new HttpGet(url);
		for (int ii = 0; ii < 100; ii++) {
			CloseableHttpClient httpClient = httpClientBuilder.build();
			CloseableHttpResponse httpResponse = null;
			try {
				long localStart = System.currentTimeMillis();
				httpResponse = httpClient.execute(get);
				System.out.println("httpClient实例是："+httpClient+"，目标网站返回http状态为：" + httpResponse.getStatusLine().getStatusCode() + "，历时"
						+ (System.currentTimeMillis() - localStart) + "毫秒");
			} catch (IOException e) {
				e.printStackTrace();
			}finally {
				if(httpResponse != null){
					try {
						//必须关闭response，不然会报： Timeout waiting for connection from pool  异常
						httpResponse.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}
		System.out.println("总历时" + (System.currentTimeMillis() - start) + "毫秒");
	}
	
	

	/**
	 * 普通的httpclient请求，同一个httpclient循环请求同一个网址
	 * <br>此种方案会有 Timeout waiting for connection from pool 问题
	 */
	@Test
	public void simpleTestAA() {
		//设置重试方案
		HttpRequestRetryHandlerImpl retry = new HttpRequestRetryHandlerImpl();
		
		// 设置超时时间
		RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(1000).setSocketTimeout(1000).setConnectionRequestTimeout(1000).build();
		
		//获取httpclient
		CloseableHttpClient httpClient = HttpClients.custom().setDefaultRequestConfig(requestConfig).setRetryHandler(retry).build();


		long start = System.currentTimeMillis();

		// 循环请求统一个网址，使用同一个http客户端
		HttpGet get = new HttpGet(url);
		for (int ii = 0; ii < 20; ii++) {
			try {
				long localStart = System.currentTimeMillis();
				CloseableHttpResponse httpResponse = httpClient.execute(get);
				System.out.println("httpClient实例是："+httpClient+"，目标网站返回http状态为：" + httpResponse.getStatusLine().getStatusCode() + "，历时"	+ (System.currentTimeMillis() - localStart) + "毫秒");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("总历时" + (System.currentTimeMillis() - start) + "毫秒");
	}
	
	/**
	 * 普通的httpclient请求，每一个都是全新的httpclient循环请求同一个网址
	 */
	@Test
	public void simpleTestBB() {
		
		HttpRequestRetryHandlerImpl retry = new HttpRequestRetryHandlerImpl();
		// 设置超时时间
		RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(1000).setSocketTimeout(1000).setConnectionRequestTimeout(1000).build();
		


		long start = System.currentTimeMillis();

		// 循环请求统一个网址，使用同一个http客户端
		HttpGet get = new HttpGet(url);
//		get.setConfig(requestConfig);
		for (int ii = 0; ii < 20; ii++) {
			CloseableHttpClient httpClient = HttpClients.custom().setDefaultRequestConfig(requestConfig).setRetryHandler(retry).build();
			try {
				long localStart = System.currentTimeMillis();
				CloseableHttpResponse httpResponse = httpClient.execute(get);
				System.out.println("httpClient实例是："+httpClient+"，目标网站返回http状态为：" + httpResponse.getStatusLine().getStatusCode() + "，历时"+ (System.currentTimeMillis() - localStart) + "毫秒");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally {
				try {
					httpClient.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		System.out.println("总历时" + (System.currentTimeMillis() - start) + "毫秒");
	}
	
	/**
	 * HttpRequestRetryHandler测试，只有抛出异常时才触发重试机制？？
	 */
	@Test
	public void retrySimpleTestAA(){
		HttpRequestRetryHandlerImpl retry = new HttpRequestRetryHandlerImpl();
		CloseableHttpClient httpClient = HttpClients.custom().setRetryHandler(retry).build();
		HttpGet get = new HttpGet("http://127.0.0.1:808");//设置一个错误的URL
		try {
			CloseableHttpResponse httpResponse = httpClient.execute(get);
			int statusCode = httpResponse.getStatusLine().getStatusCode();
			System.out.println(statusCode);
		} catch (IOException e) {
			System.out.println("不打印异常");
		}
	}
	
	/**
	 * 缓存httpclient response inputstream到本地
	 */
	@Test
	public void simpleInputstreamTestAA(){
		InputStream inputStream = this.getInputStream();
		try {
			System.out.println("输入流字节长度为："+inputStream.available());
			System.out.println(IOUtils.toString(inputStream,"UTF-8"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private InputStream getInputStream(){
		HttpRequestRetryHandlerImpl retry = new HttpRequestRetryHandlerImpl();
		CloseableHttpClient httpClient = HttpClients.custom().setRetryHandler(retry).build();
		HttpGet get = new HttpGet(url);//设置一个错误的URL
		CloseableHttpResponse httpResponse = null;
		try {
			httpResponse = httpClient.execute(get);
			int statusCode = httpResponse.getStatusLine().getStatusCode();
			System.out.println(statusCode);
			
			//缓存输入流
			InputStream inputStreamServer = httpResponse.getEntity().getContent();
			
			//获取临时目录
			File tempDirectory = FileUtils.getTempDirectory();
			if(!tempDirectory.exists()){
				tempDirectory.mkdirs();
			}
			
			//将网络输入流缓存到临时目录
			File tempFile = new File(tempDirectory.getPath()+File.separator+"hm"+File.separator+UUID.randomUUID());
			System.out.println("临时缓存文件路径："+tempFile);
			FileOutputStream cacheOutputStream = FileUtils.openOutputStream(tempFile);
			IOUtils.copy(inputStreamServer, cacheOutputStream);
			
			BufferedInputStream bis = new BufferedInputStream(FileUtils.openInputStream(tempFile));
			return bis;
		} catch (IOException e) {
			System.out.println("不打印异常");
			throw new RuntimeException(e);
		}finally {
			if(httpResponse!=null){
				try {
					httpResponse.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
