package top.hmtools.wxmp.core.access_handle;

import org.junit.Test;

import com.alibaba.fastjson.JSON;

import top.hmtools.wxmp.AppId;
import top.hmtools.wxmp.core.configuration.AppIdSecretBox;
import top.hmtools.wxmp.core.configuration.AppIdSecretPair;
import top.hmtools.wxmp.core.model.AccessTokenBean;

public class DefaultAccessTokenHandleTest {

	/**
	 * 测试获取access token示例
	 */
	@Test
	public void testGetAccessTokenString() {
		//设置获取appid、appsecret数据对的存储盒子
		AppIdSecretBox appIdSecretBox = new AppIdSecretBox() {
			
			@Override
			public AppIdSecretPair getAppIdSecretPair() {
				AppIdSecretPair appIdSecretPair = new AppIdSecretPair();
				appIdSecretPair.setAppid(AppId.appid);
				appIdSecretPair.setAppsecret(AppId.appsecret);
				return appIdSecretPair;
			}
		};
		
		//初始化 获取access token信息的对象实例
		DefaultAccessTokenHandle accessTokenHandle = new DefaultAccessTokenHandle(appIdSecretBox);
		
		//请求微信服务器接口，获取access token 信息
		AccessTokenBean accessTokenBean = accessTokenHandle.getAccessTokenBean();
		
		System.out.println(JSON.toJSONString(accessTokenBean));
	}

}
