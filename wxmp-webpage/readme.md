[![Maven Central](https://img.shields.io/maven-central/v/top.hmtools/wxmp-webpage.svg?label=Maven%20Central)](https://search.maven.org/search?q=g:%22top.hmtools%22%20AND%20a:%22wxmp-webpage%22)
#### 前言
- 本组件主要对应[微信网页开发](https://developers.weixin.qq.com/doc/offiaccount/OA_Web_Apps/Wechat_webpage_authorization.html)进行封装。

#### 引用jar包
```
<dependency>
  <groupId>top.hmtools</groupId>
  <artifactId>wxmp-webpage</artifactId>
  <version>1.0.0</version>
</dependency>
```

#### 网页授权
- 相关工具封装在`top.hmtools.wxmp.webpage.authorize.OAuth2Tools`
- 使用示例参照：[OAuth2ToolsTest.java](src/test/java/top/hmtools/wxmp/webpage/authorize/OAuth2ToolsTest.java)

#### js-sdk
- 获取js-sdk ticket：[IJsSdkApiTest.java](src/test/java/top/hmtools/wxmp/webpage/apis/IJsSdkApiTest.java)
- 初始化 HTML中的 js-sdk.js 示例可参照：[wxmp-server-demo组件中的示例HTML](../wxmp-server-demo/src/main/resources/public/index.html)





2020.05.31  微信网页开发组件总体全部测试OK（js-sdk通过HTML页面仅测试初始化成功）