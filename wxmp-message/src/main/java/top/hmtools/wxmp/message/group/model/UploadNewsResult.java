package top.hmtools.wxmp.message.group.model;

import top.hmtools.wxmp.core.model.ErrcodeBean;
import top.hmtools.wxmp.core.model.message.enums.MsgType;

/**
 * Auto-generated: 2019-08-26 9:57:8
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class UploadNewsResult extends ErrcodeBean{

	private MsgType type;
	private String media_id;
	private long created_at;


	public MsgType getType() {
		return type;
	}

	public void setType(MsgType type) {
		this.type = type;
	}
	
	public void setType(String msgType) {
		if (msgType == null || msgType.length() < 1) {
			return;
		}

		MsgType[] values = MsgType.values();
		for (MsgType ee : values) {
			if (ee.getName().equals(msgType)) {
				this.type = ee;
				break;
			}
		}
	}

	public void setMedia_id(String media_id) {
		this.media_id = media_id;
	}

	public String getMedia_id() {
		return media_id;
	}

	public void setCreated_at(long created_at) {
		this.created_at = created_at;
	}

	public long getCreated_at() {
		return created_at;
	}

}