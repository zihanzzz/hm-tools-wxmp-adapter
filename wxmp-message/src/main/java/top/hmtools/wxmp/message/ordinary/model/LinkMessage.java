package top.hmtools.wxmp.message.ordinary.model;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.annotations.XStreamAlias;

import top.hmtools.wxmp.core.annotation.WxmpMessage;
import top.hmtools.wxmp.core.model.message.BaseMessage;
import top.hmtools.wxmp.core.model.message.enums.MsgType;

/**
 * 链接消息
 * {@code
 * <xml>
	<ToUserName><![CDATA[toUser]]></ToUserName>
	<FromUserName><![CDATA[fromUser]]></FromUserName>
	<CreateTime>1351776360</CreateTime>
	<MsgType><![CDATA[link]]></MsgType>
	<Title><![CDATA[公众平台官网链接]]></Title>
	<Description><![CDATA[公众平台官网链接]]></Description>
	<Url><![CDATA[url]]></Url>
	<MsgId>1234567890123456</MsgId>
</xml>
 * }
 * @author HyboWork
 *
 */
@WxmpMessage(msgType=MsgType.link)
public class LinkMessage extends BaseMessage{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7329659443556249327L;

	/**
	 * 消息标题
	 */
	@XStreamAlias("Title")
	private String title;
	
	/**
	 * 消息描述
	 */
	@XStreamAlias("Description")
	private String description;
	
	/**
	 * 消息链接
	 */
	@XStreamAlias("Url")
	private String url;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}


	@Override
	public void configXStream(XStream xStream) {
		
	}
	
	
}
