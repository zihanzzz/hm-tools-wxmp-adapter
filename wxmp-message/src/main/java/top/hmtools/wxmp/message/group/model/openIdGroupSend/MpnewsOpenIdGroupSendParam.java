package top.hmtools.wxmp.message.group.model.openIdGroupSend;

/**
 * 图文消息（注意图文消息的media_id需要通过上述方法来得到）：
 * @author HyboWork
 *
 */
public class MpnewsOpenIdGroupSendParam extends BaseOpenIdGroupSendParam {

	private MediaId mpnews;
	
	private int send_ignore_reprint;

	public MediaId getMpnews() {
		return mpnews;
	}

	public void setMpnews(MediaId mpnews) {
		this.mpnews = mpnews;
	}

	public int getSend_ignore_reprint() {
		return send_ignore_reprint;
	}

	public void setSend_ignore_reprint(int send_ignore_reprint) {
		this.send_ignore_reprint = send_ignore_reprint;
	}

	@Override
	public String toString() {
		return "MpnewsOpenIdGroupSendParam [mpnews=" + mpnews + ", send_ignore_reprint=" + send_ignore_reprint
				+ ", msgtype=" + msgtype + ", touser=" + touser + "]";
	}
	
	
}
